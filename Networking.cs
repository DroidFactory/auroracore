using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace NetworkTube
{
    public class NTube  // NetworkTubeBase  -->  NetworkTube  -->  NTube
    {
        /* Discussion:
         * 
         * - Worth reading:  http://msdn2.microsoft.com/en-us/library/w2a9a9s3.aspx  (delegate-link and stuff)  /JD  25/11-06
         * 
         * - Let's make this class a "base-class", and make a new class that inherit this one in the other projects.
         *   That way we can add events on special things (ex. like incoming_chatt-object) that has to do with the 
         *   program we're working on.  /JD  26/11-06
         *
         * - NOT! :P  /JD 04/12-06
         * 
         */

        // ** Private members
        Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        byte[] buff = new byte[8024];
        int sent = 0, received = 0;
        string thisSockName = "", thisSockDescription = "";
        string remoteAddr;
        // 2 vars for socket buffering
        string buff_IN;
        bool readyForAction = true;

        // ** Public members
        //public delegate void objIncoming_callback(object me);  // Delegate to use in a derived class for object-sending  - just a "sample"
        //public event objIncoming_callback Incoming_Object;     // Event - add function (void bleh(object hi)) to trigger then this object comes in

        public delegate void dele_incoming_object(string obj_name, string obj_xml, NTube tube);
        public event dele_incoming_object event_incoming_object;

        public delegate void dele_incoming_connection(NTube tube, IAsyncResult iasync);
        public event dele_incoming_connection event_incoming_connection;

        public delegate void dele_tube_disconnected(NTube tube);
        public event dele_tube_disconnected event_tube_disconnected;


        // ** Constructor
        public NTube()
        {
        }
        public NTube(string connectionName, string connectionDesc)   // Allows to set a name and desc on the connection when initialized
        {
            thisSockName = connectionName;
            thisSockDescription = connectionDesc;
        }
        public NTube(Socket already_active_socket)  // Will be based on this socket
        {
            sock = already_active_socket;
            remoteAddr = sock.RemoteEndPoint.ToString();
        }

        // ** Private methods
        // incoming_connection_from_listening - This is triggerd when the tube is listening and someone is connecting
        void incoming_connection_from_listening(IAsyncResult res)
        {
            //Socket income = (res.AsyncState as NetworkTubeBase).sock.EndAccept(res);  // I hope "res" here is unimportant .. Tried null and stuff first, then just took the IAsyncResault res :b
            event_incoming_connection.Invoke(this, res);  // All functions bound to this event are triggerd
            // Leave the decission of starting a new async accepting to the attached function(s)
        }

        // incoming_data_on_socket - Trigger when data arrives on the socket
        void incoming_data_on_socket(IAsyncResult res)
        {
            try
            {
                sock.EndReceive(res);
                //start_async_read();  // Continue reading
            }
            catch
            {
                // If socket got disconnected, invoke that event instead of craching :P
                event_tube_disconnected.Invoke(this);
                return;
            }

            if (!(sock.Available > 0))
            {
                start_async_read();
                return;
            }

            fillBuffIn();
            if (readyForAction)
                actionOnTube();
            //else
            //MessageBox.Show("not ready for action..");
        }

        void actionOnTube()
        {
            readyForAction = false;
            //MessageBox.Show("action!  just nu �r det " + buff_IN.Length);
            // Incoming data on this tube - read type-of-data and do what's meant .. Eh, I think my english just flew out the window 
            
            string type = read_buff(3);
            switch (type)  // Read first 3 chars (let's call it type of event)
            {
                case "OBJ":   //  OBJ-syntax:   3char OBJ 2char len_name Name-of-object 5char XML-len len_char XML-object
                    // Get object
                    int objNameLen = Convert.ToInt32(read_buff(2));
                    string obj_name = read_buff(objNameLen);

                    string len_str = read_buff(5);
                    int xml_len = Convert.ToInt32(len_str);
                    string xml_obj = read_buff(xml_len);
                    event_incoming_object(obj_name, xml_obj, this);  // Trigger the attached functions that want the object
                    break;
                case "COM":   //  COM-syntax:   3char COM 4char command
                    // Do command
                    switch (read_buff(4))
                    {
                        case "Ping":  // Ping is a command (COM) .. Let's act right away without disturbing the overlying application
                            this.raw_send("COMPong\n");  // Reply with Pong - let's not forget always-needed \n at the end
                            break;
                    }
                    break;
                case "DIS":
                    // Socket is disconnecting
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();                    
                    event_tube_disconnected.Invoke(this);
                    return;
                default:
                    MessageBox.Show("Something just went bad with the connection :( - Didn't get a recognizable type-of-event on the tube");
                    return;
            }

            string check = read_buff(1);
            if (!(check == "\n"))
            {
                MessageBox.Show("Error! Didn't get the confirming newline in the tube! (got: " + check + ")");
                throw new Exception("Error in tube");
            }

            if (buff_IN.Length > 0)
            {
                //MessageBox.Show("(let's go) buff: " + buff_IN);
                actionOnTube();
            }

            readyForAction = true;
            start_async_read();
        }

        string read_sock(int length)
        {
            //MessageBox.Show("read_sock");
            if (!sock.Connected) return null;
            byte[] buff = new byte[length * 2];  // It's all in UTF-16 (Unicode), so there's 2 byte for every character

            try
            {
                sock.Receive(buff);
            }
            catch
            {
                event_tube_disconnected.Invoke(this);
            }

            string ret = Encoding.Unicode.GetString(buff).Trim(new char[] {'\0'});

            return ret;
        }

        string read_buff(int len)
        {
            //MessageBox.Show("read_buff");
            int loops = 0;
            while (buff_IN.Length < len)
            {
                //MessageBox.Show("want: " + len.ToString() + " .. have: " + buff_IN.Length.ToString());
                fillBuffIn();
                if (loops > 1)
                    System.Threading.Thread.Sleep(50);
                if (loops > 40)
                {
                    MessageBox.Show("(Tub-trubbel): motparten lovade att skicka data, men gjorde inte det p� 2sek.. taskigt");
                    return "";
                }
                loops++;
            }
            string ret = buff_IN.Substring(0, len);
            buff_IN = buff_IN.Substring(len);
            return ret;
        }

        void fillBuffIn()
        {
            //MessageBox.Show("Fillbuffin..");
            int a = sock.Available / 2;
            //MessageBox.Show("avail: " + a.ToString());
            if (a > 0)
                buff_IN += read_sock(a);
        }

        // ** Public methods
        // connect - This NetworkTube will connect to a host using IP/DNS and port
        public void connect(string target, int port)
        {
            remoteAddr = target + ":" + port.ToString();
            try
            {
                IPAddress target_ip = (Dns.GetHostAddresses(target))[0];
                sock.Connect(target_ip, port);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("connect() error: " + ex.Message);
                throw (new Exception("Can't connect", ex));
            }
        }

        // disconnect - disconnect nicely
        public void disconnect()
        {
            raw_send("DIS\n");
            sock.Shutdown(SocketShutdown.Both);
            sock.Close();
            //sock.Shutdown(SocketShutdown.Both);
        }

        // read_on_socket - Starts an async read on the socket
        public void start_async_read()
        {
            try
            {
                sock.BeginReceive(new byte[0], 0, 0, SocketFlags.None, new AsyncCallback(incoming_data_on_socket), null);  // Just trigger function on incoming data - nothing else
            }
            catch
            {
                event_tube_disconnected.Invoke(this);
            }
        }

        // stop_async_read - Needed because we have to get the socket from outside on the async-accepting
        public Socket get_connecting_socket(IAsyncResult iasync)
        {
            return sock.EndAccept(iasync);
        }

        public void start_async_accept()
        {
            sock.BeginAccept(new AsyncCallback(incoming_connection_from_listening), this);
        }

        // listen_and_accept - This tube will be a basic listen-and-accept-server. On connection the event incominc_connection will be triggerd
        public void listen_and_accept(int port)
        {
            try
            {
                sock.Bind(new IPEndPoint(IPAddress.Any, port));
                sock.Listen(10);
                start_async_accept();
            }
            catch (Exception ex)
            {
                MessageBox.Show("listen() error: " + ex.Message);
                throw (new Exception("Can't listen", ex));
            }
        }


        // raw_send - What you put in me comes out the other end :: Takes byte[] or string
        public void raw_send(byte[] msg)
        {
            //if (sock.Connected)
            //{
            try
            {
                sock.Send(msg);
                sent += msg.GetLength(0);
            }
            catch
            {
                event_tube_disconnected.Invoke(this);
            }
            //}
            //else
            //    throw new Exception("Tried to send() on a socket that's not connected to a remote host >:(");
        }
        public void raw_send(String msg)
        {
            byte[] bMsg = Encoding.Unicode.GetBytes(msg);

            raw_send(bMsg);
        }
        // - End of raw_send


        
        // http://www.thescripts.com/forum/thread258432.html
        // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp09182003.asp  Don't read while sleepy
        /// <summary>
        /// Sends all public parts of an object to the other side (with some headers so the other side knows what's coming)
        /// </summary>
        /// <typeparam name="T">The class of the object</typeparam>
        /// <param name="obj_name">Name of the class (for identification on other side)</param>
        /// <param name="obj">The object</param>
        public void send_object<T>(T obj) where T : class
        {
            string obj_name = obj.GetType().Name;

            StringWriter sw = new StringWriter();
            XmlSerializer xml_seri = new XmlSerializer(typeof(T));

            xml_seri.Serialize(sw, obj);

            string obj_xml = sw.ToString();

            this.raw_send("OBJ" + x_char_len_of_string(obj_name, 2) + obj_name + x_char_len_of_string(obj_xml, 5) + obj_xml + "\n");
            // This looks kind of like this on the network:
            // OBJ04Name00012<XML-Code />\n
            // The \n on the end is a real newline, not slash-N
        }
        // - End of send_object

        // get_object - Gets the object from XML
        /// <summary>
        /// Convert XML into an object
        /// </summary>
        /// <typeparam name="T">The class of the object</typeparam>
        /// <param name="obj_xml">The XML-code</param>
        /// <returns>Object of the class - converted from XML</returns>
        public static T get_object<T>(string obj_xml) where T : class
        {
            StringReader sw = new StringReader(obj_xml);
            XmlSerializer xml_seri = new XmlSerializer(typeof(T));

            try
            {
                return xml_seri.Deserialize(sw) as T;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Exception! :(");
                return null;
            }
        }

        // get_object again, but non-static (so it can be seen from a tube-object too)
        /// <summary>
        /// Convert XML into an object
        /// </summary>
        /// <typeparam name="T">The class of the object</typeparam>
        /// <param name="obj_xml">The XML-code</param>
        /// <returns>Object of the class - converted from XML</returns>
        public T get_object_<T>(string obj_xml) where T : class
        {
            return NTube.get_object<T>(obj_xml);
        }

        // ** Properties
        public string remoteIP  // Returns the (string) remote IP
        { get { return remoteAddr; } }

        public int bytesSent // Gets how many (int) bytes sent on the socket
        { get { return sent; } }

        public int bytesReceived // Gets how many (int) bytes received on the socket
        { get { return received; } }

        public string connectionName // Get or set the name of the connection  (useful ex. in a ListBox or a PropertyGrid)
        {
            get { return thisSockName; }
            set { thisSockName = value; }
        }

        public string connectionDescription // Get or set the description of the socket
        {
            get { return thisSockDescription; }
            set { thisSockDescription = value; }
        }

        public Socket socket // So that the socket can be accessed from outside (needed ex. when accepting connections)
        {
            get { return sock; }
        }

        public bool Connected
        {
            get
            {
                return sock.Connected;
            }
        }


        // This function returns the length of the string in a five-char (ex.  00042) format
        public static string x_char_len_of_string(string me, int x_length)
        {
            string ret = "";
            for (int i = x_length; i != 0; i--)
                if (me.Length.ToString().Length < i)
                    ret += "0";
                else
                {
                    ret += me.Length.ToString();
                    break;
                }
            return ret;
        }
    }
}